// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from './firebase'
import store from './store'

window.firebase = firebase;

Vue.config.productionTip = false;

require('./../semantic/dist/semantic.css');
require('./../semantic/dist/semantic.js');

/* eslint-disable no-new */

const unsuscribe = firebase.auth().onAuthStateChanged(user => {

    store.dispatch('setUser', user);

    new Vue({
        el: '#app',
        router,
        store,
        template: '<App/>',
        components: {App}
    });

    unsuscribe();

});
