import Vue from 'vue'
import Router from 'vue-router'

import Tchat from '@/pages/Tchat'
import Login from '@/pages/Login'
import Register from '@/pages/Register'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Tchat',
            component: Tchat,
            beforeEnter(from, to, next){
                if(!firebase.auth().currentUser){
                    next('/login');
                }else{
                    next();
                }
            }
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/register',
            name: 'Register',
            component: Register
        }
    ]
})
