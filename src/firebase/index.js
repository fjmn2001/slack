import firebase from 'firebase'

let config = {
    apiKey: "AIzaSyCbtcDfYKtK20qFiTNggE55dXIpYTAN53g",
    authDomain: "slack-c141a.firebaseapp.com",
    databaseURL: "https://slack-c141a.firebaseio.com",
    projectId: "slack-c141a",
    storageBucket: "slack-c141a.appspot.com",
    messagingSenderId: "129215568641"
};

firebase.initializeApp(config);

export default firebase;
